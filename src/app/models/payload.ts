export class Payload {
  constructor(
    public age_certifications: Array<String>,
    public content_types: Array<String>,
    public presentation_types: Array<String>,
    public providers: Array<String>,
    public genres: Array<String>,
    public languages: Array<String>,
    public release_year_from: Number,
    public release_year_until: Number,
    public monetization_types: Array<String>,
    public min_price: Number,
    public max_price: Number,
    public nationwide_cinema_releases_only: Boolean,
    public scoring_filter_types: Array<String>,
    public cinema_release: String,
    public query: String,
    public page: Number,
    public page_size: Number,
    public timeline_type: String
  ) {}
}
