export class Provider {
  constructor(
    public id: number,
    public profile_id: number,
    public technical_name: string,
    public short_name: string,
    public clear_name: string, ) {}
}
