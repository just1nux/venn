import { Component, Input, OnInit } from '@angular/core';
import { Provider } from '../models/provider';
import { ProvidersService } from '../services/providers.service';
import { TvMovieService } from '../services/tvmovie.service';

@Component({
  selector: 'app-provider-list',
  template: `
    <select class="form-control" id="provider" (change)="changeProvider()"
            [(ngModel)]="selectedProvider.id" name="name">
      <option *ngFor="let provider of providers" [value]="provider.id">{{provider.clear_name}}</option>
    </select>`
})
export class ProviderListComponent implements OnInit {

  constructor(private providersService: ProvidersService, private tvMovieService: TvMovieService) { }
  @Input() sequence: number;
  providers: Provider[];
  selectedProvider = new Provider(0, 0, '', '', '');

  ngOnInit() {
    this.providers = this.providersService.getProviders();
  }

  changeProvider() {
    const val: number = Number(this.selectedProvider.id);
    this.tvMovieService.selectedProviderChanged(val, this.sequence);
  }

}
