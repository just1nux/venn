import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ProvidersService } from './services/providers.service';
import { ProviderListComponent } from './provider-list/provider-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TvMovieService } from './services/tvmovie.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ProviderListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [ProvidersService, TvMovieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
