import { Injectable } from '@angular/core';
import { Provider } from '../models/provider';
import { Media } from '../models/media';
import { Payload } from '../models/payload';
import { ProvidersService } from './providers.service';
import * as d3 from 'd3';
import * as venn from 'venn.js';
import * as request from 'superagent';


@Injectable()
export class TvMovieService {

  private mediaProviders = [];
  private searches = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];
  private chart = venn.VennDiagram();
  private sets = [];
  private state = false;

  private providerA: Provider = new Provider(0, 0, '', '', '');
  private providerB: Provider = new Provider(0, 0, '', '', '');
  private providerC: Provider = new Provider(0, 0, '', '', '');

  private selectedProviders: Provider[] = [this.providerA, this.providerB, this.providerC];

  constructor(private providerService: ProvidersService) {
    this.retrievePopular();
  }

  public refreshVenn(A, B, C, AB, BC, AC) {

    const aName = this.selectedProviders[0].clear_name;
    const bName = this.selectedProviders[1].clear_name;
    const cName = this.selectedProviders[2].clear_name;

    this.sets = [
      {sets: [aName], size: A},
      {sets: [bName], size: B},
      {sets: [cName], size: C},
      {sets: [aName, bName], size: AB},
      {sets: [bName, cName], size: BC},
      {sets: [cName, aName], size: AC}
    ];

    d3.select('#venn').datum(this.sets).call(this.chart);

    this.state = false;
  }

  public selectedProviderChanged(id, seq) {
    this.selectedProviders[seq] = this.providerService.getProviderById(id);
    this.totalProviders();
  }

  public totalProviders() {
    if (!this.state) {

      this.state = true;

      let A = 0;
      let B = 0;
      let C = 0;
      let AB = 0;
      let BC = 0;
      let AC = 0;

      const provider0 = this.selectedProviders[0].id;
      const provider1 = this.selectedProviders[1].id;
      const provider2 = this.selectedProviders[2].id;

      for (const p of this.mediaProviders) {
        // stop any that are already running

        if (p.providers.includes(provider0)) {
          A++;
        }
        if (p.providers.includes(provider1)) {
          B++;
        }
        if (p.providers.includes(provider2)) {
          C++;
        }
        if (p.providers.includes(provider0) && p.providers.includes(provider1)) {
          AB++;
        }
        if (p.providers.includes(provider1) && p.providers.includes(provider2)) {
          BC++;
        }
        if (p.providers.includes(provider0) && p.providers.includes(provider2)) {
          AC++;
        }
      }

      this.refreshVenn(A, B, C, AB, BC, AC);
    }
  }

  public retrievePopular() {

    const query = this.searches.pop();

    const payload = new Payload(null, ['show', 'movie'], null,
     null, null, null, null, null,
    null, null, null, null,
      null, null, query, 1, 100, null);

    request.post('https://api.justwatch.com/titles/en_US/popular')
      .send(payload)
      .then((res) => {

        for (const media of res.body.items) {
          const providers = [];
          if (media.offers) {
            for (const offer of media.offers) {
              providers.push(offer.provider_id);
            }
          }
          const mediaOffer = new Media(media.id, providers);
          this.mediaProviders.push(mediaOffer);
          this.totalProviders();
        }
      });

    // calls itself again if there are more searches to request
    if (this.searches.length > 0) {
      this.retrievePopular();
    }
  }

}
