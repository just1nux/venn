import { Injectable } from '@angular/core';
import * as request from 'superagent';
import { Provider } from '../models/provider';

@Injectable()
export class ProvidersService {
  public blankProvider = new Provider(0, 0, '', '', '');
  private providers = [];

  constructor() {
    this.providers.push(this.blankProvider);
    this.retrieveProviders();
  }

  public getProviders() {
    return this.providers;
  }

  public retrieveProviders() {
    request.get('https://api.justwatch.com/providers/locale/en_US')
      .then((res) => {
        for (const prov of res.body) {
          const provider = new Provider(prov.id, prov.profile_id, prov.technical_name, prov.short_name, prov.clear_name);
          this.providers.push(provider);
        }
      });
  }

  public getProviderById(id): Provider {
    let p: Provider = null;

    for (const provider of this.providers) {
      const p1 = parseInt(provider.id, 10);
      if (p1 === id) {
        p = provider;
        break;
      }
    }

    return p;
  }

}
